# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8 compliant>

# Script copyright (C) Campbell Barton


def power_of_2_max_u(x):
    x -= 1
    x |= (x >> 1)
    x |= (x >> 2)
    x |= (x >> 4)
    x |= (x >> 8)
    x |= (x >> 16)
    return x + 1


def sub_v2v2(a, b):
    return (
        a[0] - b[0],
        a[1] - b[1])


def dot_v2v2(a, b):
    return (
        (a[0] * b[0]) +
        (a[1] * b[1]))


def line_point_factor_v2(p, l1, l2, default=0.0):
    u = sub_v2v2(l2, l1)
    h = sub_v2v2(p, l1)
    dot = dot_v2v2(u, u)
    return (dot_v2v2(u, h) / dot) if dot != 0.0 else default


def isect_seg_seg_v2_point(v1, v2, v3, v4):
    div = (v2[0] - v1[0]) * (v4[1] - v3[1]) - (v2[1] - v1[1]) * (v4[0] - v3[0])
    if div == 0.0:
        return None

    vi = (
        ((v3[0] - v4[0]) * (v1[0] * v2[1] - v1[1] * v2[0]) - (v1[0] - v2[0]) * (v3[0] * v4[1] - v3[1] * v4[0])) / div,
        ((v3[1] - v4[1]) * (v1[0] * v2[1] - v1[1] * v2[0]) - (v1[1] - v2[1]) * (v3[0] * v4[1] - v3[1] * v4[0])) / div,
        )

    fac = line_point_factor_v2(vi, v1, v2, default=-1.0)
    if fac < 0.0 or fac > 1.0:
        return None

    fac = line_point_factor_v2(vi, v3, v4, default=-1.0)
    if fac < 0.0 or fac > 1.0:
        return None

    return vi


class Box:
    __slots__ = (
        "range_x",
        "range_y",
        )


class Node:
    __slots__ = (
        "box",
        "child_pair",  # (Node, Node)
        "line"  # -1
        )

    def __init__(self):
        self.box = Box()
        self.child_pair = [None, None]
        self.line = -1


def box_init_from_point_pair(box, a, b):
    box.range_x = [min(a[0], b[0]), max(a[0], b[0])]
    box.range_y = [min(a[1], b[1]), max(a[1], b[1])]


def box_init_from_box(box, a):
    box.range_x = a.range_x[:]
    box.range_y = a.range_y[:]


def box_init_from_box_pair(box, a, b):
    box.range_x = [min(a.range_x[0], b.range_x[0]), max(a.range_x[1], b.range_x[1])]
    box.range_y = [min(a.range_y[0], b.range_y[0]), max(a.range_y[1], b.range_y[1])]


def box_isect(a, b):
    return not (
        (a.range_x[0] >= b.range_x[1]) or
        (a.range_x[1] <= b.range_x[0]) or
        (a.range_y[0] >= b.range_y[1]) or
        (a.range_y[1] <= b.range_y[0]))

VALIDATE = False


def isect_polygon(points) -> list:

    # pre-allocate array
    nodes = [None] * power_of_2_max_u(len(points)) * 2

    i_prev = len(points) - 1
    for i_curr in range(len(points)):
        n = Node()

        box_init_from_point_pair(n.box, points[i_curr], points[i_prev])
        n.line = i_prev

        nodes[i_curr] = n
        i_prev = i_curr

    # -------------------------------------------------------------------------
    # Build Tree Depth

    nodes_len = len(points)

    i_start = 0
    i_end = nodes_len

    while i_start + 1 != i_end:
        i_curr = i_start
        # Ensure stepping by 2's wont go past i_end
        i_end_roundup = i_end + ((i_end - i_curr) % 2)
        while i_curr != i_end_roundup:
            assert(i_curr < i_end_roundup)
            n = Node()

            i_next = i_curr + 1
            if i_next != i_end:
                n.child_pair[0] = nodes[i_curr]
                n.child_pair[1] = nodes[i_next]
                box_init_from_box_pair(n.box, n.child_pair[0].box, n.child_pair[1].box)
            else:
                # We could avoid having a node here
                # but then we don't have fixed depth to leaf nodes
                n.child_pair[0] = nodes[i_curr]
                box_init_from_box(n.box, n.child_pair[0].box)

            nodes[nodes_len] = n
            nodes_len += 1

            i_curr += 2

        # set a new span to fill in the parent
        # nodes to the pass just finished.
        i_start = i_end
        i_end = nodes_len
    del n

    root = nodes[nodes_len - 1]

    if VALIDATE:
        def _():
            edge_set = set()

            def recurse_nodes(n):
                for c in n.child_pair:
                    if c is not None:
                        if c.line != -1:
                            assert(c.line not in edge_set)
                            edge_set.add(c.line)
                        else:
                            recurse_nodes(c)
            recurse_nodes(root)
            assert(len(edge_set) == len(points))
        _(); del _

    # -------------------------------------------------------------------------
    # find all intersections

    isect = []

    def find_isect_recursive(n_a, n_b):
        if n_a.line != -1:
            # Ensure depths are always equal (both leaf nodes in this case)
            assert(n_b.line != -1)
            vi = isect_seg_seg_v2_point(
                    points[n_a.line], points[(n_a.line + 1) % len(points)],
                    points[n_b.line], points[(n_b.line + 1) % len(points)],
                    )
            if vi is not None:
                isect.append(vi)
        else:
            for n_a_child in n_a.child_pair:
                if n_a_child is not None and box_isect(n_a_child.box, n_b.box):
                    for n_b_child in n_b.child_pair:
                        if n_b_child is not None and box_isect(n_a_child.box, n_b_child.box):
                            find_isect_recursive(n_a_child, n_b_child)

    def do_child_intersect(n):
        if n.child_pair[0] is None:
            pass
        elif n.child_pair[1] is None:
            do_child_intersect(n.child_pair[0])
        elif box_isect(n.child_pair[0].box, n.child_pair[1].box):
            find_isect_recursive(n.child_pair[0], n.child_pair[1])
            do_child_intersect(n.child_pair[0])
            do_child_intersect(n.child_pair[1])

    do_child_intersect(root)

    return isect
