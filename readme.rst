*************************
Bintree Line Intersection
*************************

This is an experimental script for finding all line intersections
focused on contiguous hand lines (such as hand drawn shapes).

Algorithmically this will be far worse than **Bentley Ottmann** for example.
But in the cases where lines aren't a random set of edges, the performance may be acceptable.

The method used is very straightforward.

There is nothing especially novel or interesting about this,
its mainly as an example of a very simple spatial optimization
which better methods can be compared to.

- Build a binary tree of edges.
- Recursively intersect AABB and finally the lines once the leaf nodes are reached,
  where each level recursively checks for intersections
  but only between its 2 branches.

Pros
====

- Simple to implement.
- Users arrays/plain data types (no balanced trees or sorted sets).
- Performs acceptably with contiguous lines (and drawn lines for example).

Cons
====

- Worst case is approximately as bad as checking all possible combinations.
  However this could be supported effeciently using a BVH with spatial splits.
- Lines that loop back on themselves often wont be accessed efficiently in the tree structure.
- This implementation relies on a continuous line (polygon),
  *though multiple polygons could be supported.*

.. note::

   This is intentionally written to be portable to C,
   which is why some of Python's features aren't used.

